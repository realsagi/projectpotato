app.get('/create-user', function(req, res){
	if(req.login.username == undefined){
		res.redirect('/login')
	}
	else if(req.login.typegroup == "employee"){
		res.render('em',{
			username: req.login.username
		});
	}
	else if(req.login.typegroup == "customer"){
		res.render("custo",{
			username: req.login.username
		});
	}
	else if(req.login.typegroup == "admin"){
		res.render('admin',{
			username: req.login.username
		});
	}
	else{
		
	}
});

app.post('/create-user-save', function(req, res){
	var user = require('./model/user');
	user.username = req.body.username;
	user.password = req.body.password;
	user.name = req.body.name;
	user.tel = req.body.tel;
	user.address = req.body.address;
	user.typegroup = req.body.typegroup;
	user.save(function(resual){
		res.send(resual);
	})
});

app.post('/create-user-findbyuser', function(req, res){
	var user = require('./model/user');
	user.username = req.body.username;
	user.findByUser(function(resual){
		res.send(resual);
	});
});

app.post('/admin/findbyFilter', function(req, res){
	var user = require('./model/user');
	user.findbyfilter = req.body.dataname;
	user.filtername(function(resual){
		res.send(resual);
	});
});