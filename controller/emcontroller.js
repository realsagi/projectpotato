app.get('/em', function(req, res){
	if(req.login.username == undefined){
		res.redirect('/')
	}
	else if(req.login.typegroup == "employee"){
		res.render('main2',{
			username: req.login.username
		});
	}
	else if(req.login.typegroup == "customer"){
		res.render("main2",{
			username: req.login.username
		});
	}
	else if(req.login.typegroup == "admin"){
		res.render('main2',{
			username: req.login.username
		});
	}
	else{
		
	}
});

app.post('/em/savebill', function(req, res){
	//console.log(req.body);
	var bills = require('./model/bills');
	bills.idbill = req.body.numbill;
	bills.userem = req.body.user;
	bills.sutsumpricebuy = req.body.licenecar;
	bills.idseller = req.body.idforseller;
	bills.nameseller = req.body.nameforseller;
	bills.dateimport = req.body.dateimport;
	bills.timeimpot = req.body.timeimport;
	bills.dateexport = req.body.dateexport;
	bills.timeexport = req.body.timeexport;
	bills.w_in_car = req.body.w_in_car;
	bills.w_out_car = req.body.w_out_car;
	bills.pricerefer = req.body.pricerefer;
	bills.percenbaby = req.body.percenbabymid;
	bills.precencal = req.body.percencal;
	bills.pricecal = req.body.pricebuy;
	bills.w_car = req.body.w_car;
	bills.sumprice = req.body.sumpricbuy;
	bills.sutsumprice = req.body.sutsumpricebuy;
	bills.save(function(returndata){
		res.send(returndata);
	});
});

app.post('/em/findAllIdbill', function(req, res){
	var bills = require('./model/bills');
	bills.findAllIdBill(function(returndata){
		res.send(returndata);
	});
});

app.post('/em/findByAll', function(req ,res){
	var bills = require('./model/bills');
	bills.findByAll(function(returndata){
		res.send(returndata);
	});
});

app.post('/em/findBybill', function(req, res){
	var bills = require('./model/bills');
	bills.idbill = req.body.idbill;
	bills.findByID(function(returndata){
		res.send(returndata);
	});
});