app.get('/login', function(req, res){
	if(req.login.typegroup == "employee"){
		res.redirect('/em')
	}
	else if(req.login.typegroup == "customer"){
		res.render("custo",{
			username: req.login.username
		});
	}
	else if(req.login.typegroup == "admin"){
		res.render('admin',{
			username: req.login.username
		});
	}
	else{
		res.render('login');
	}
});

app.post('/login/check' ,function(req, res){
	var user = require('./model/user');
	user.username = req.body.username;
	user.password = req.body.password;

	user.findByUser(function(resual){
		if(resual[0] != undefined){
			if(resual[0].username == req.body.username && resual[0].password == req.body.password){
				// ================ session =================
				req.login.username = req.body.username;
				req.login.name = resual[0].name;
				req.login.typegroup = resual[0].typegroup;
				//===========================================
				res.send(resual);
			}
		}
		else{
			res.send(false);
		}
	});
});

app.get('/login/checktypgroup', function(req, res){
	res.send(req.login.typegroup);
});

app.get('/logout', function(req,res){
	req.login.username = undefined;
	req.login.name = undefined;
	req.login.typegroup = undefined;
	res.redirect('/');
});
