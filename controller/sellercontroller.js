app.get('/create-seller', function(req, res){
	res.render("seller");
});

app.post('/create-seller/save', function(req, res){
	var idseller = req.body.idseller;
	var nameseller = req.body.nameseller;
	var addressseller = req.body.addressseller;
	var telseller = req.body.telseller;
	var registercarseller = req.body.registercarseller
	var seller = require('./model/seller');
	seller.idseller = idseller;
	seller.name = nameseller;
	seller.address = addressseller;
	seller.tel = telseller;
	seller.regiscar = registercarseller;
	seller.save(function(retrundata){
		res.send(retrundata);
	});
});

app.post('/create-seller/findbyidseller', function(req, res){
	var seller = require('./model/seller');
	seller.idseller = req.body.idseller;
	seller.findByIdseller(function(retrundata){
		if(retrundata == ""){
			res.send("yes");
		}
		else{
			res.send("no");
		}
	});
})

app.post('/create-seller/checkidseller', function(req, res){
	var seller = require('./model/seller');
	seller.idseller = req.body.idseller;
	seller.findByIdseller(function(retrundata){
		if(retrundata == ""){
			res.send("no");
		}
		else{
			res.send(retrundata);
		}
	});
})