app.post('/sendproduct/save', function(req, res){
	var sendproduct = require('./model/sendproduct');
	var bills = require('./model/bills');
	//console.log(req.body);
	sendproduct.idbills = req.body.idbills;					//int
	sendproduct.namedrive = req.body.namedrive;				// varchar(100)
	sendproduct.regiscar = req.body.regiscar;				//varchar(100)
	sendproduct.namecus = req.body.namecus;					//varchar(100)	
	sendproduct.datecusreciev = req.body.datecusreciev;		// int
	sendproduct.startpoi = req.body.startpoi;				//text
	sendproduct.stoppoi = req.body.stoppoi;					//text
	sendproduct.direction = req.body.direction;				// float
	sendproduct.pricetran = req.body.pricetran;				//float
	sendproduct.sumpricetoon = req.body.sumpricetoon;		//float
	sendproduct.sumpricesell = req.body.sumpricesell;		//float
	sendproduct.sumall = req.body.sumall;					//float
	sendproduct.save(function(returndata){
		res.send(returndata);
	});					
});

app.post('/sendproduct/findall', function(req, res){
	var sendproduct = require('./model/sendproduct');
	sendproduct.findall(function(returndata){
		res.send(returndata);
	});
});

app.post('/sendproduct/updatestatus', function(req, res){
	var sendproduct = require('./model/sendproduct');
	var aa = req.body.idq;
	for(var i = 0 ; i < aa.length ; i++){
		sendproduct.id = aa[i];
		sendproduct.updatestatus(function(returndata){
			if(returndata != "UPDATE Complate"){
				res.send(returndata);
				i = aa.length+10;
			}
		});
	}
	res.send("UPDATE Complate");
});