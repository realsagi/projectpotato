var fs = require('fs');
eval(fs.readFileSync('model/databaseconfig.js')+'');

var idbill = undefined;				//int
var userem = undefined; 			//varchar(50);
var sutsumpricebuy = undefined;		//varchar(50);
var idseller = undefined;			//varchar(50);
var nameseller = undefined;			//text;
var dateimport = undefined;			//date;
var timeimpot = undefined;			//time(7);
var dateexport = undefined;			//date;
var timeexport = undefined;			//time(7);
var w_in_car = undefined;			//float
var w_out_car = undefined;			//float
var pricerefer = undefined;			//float
var percenbaby = undefined;			//float
var precencal = undefined;			//int
var pricecal = undefined;			//float
var w_car = undefined;				//float
var sumprice = undefined;			//float
var sutsumprice = undefined;		//float
var status;							//int

exports.save = function(callback){
	var sql = 'INSERT INTO ['+database+'].[dbo].[bills] (idbill, userem, sutsumpricebuy, idseller, nameseller,'+
			  'dateimport, timeimpot, dateexport, timeexport, w_in_car, w_out_car, pricerefer, percenbaby,'+
			  'precencal, pricecal, w_car, sumprice, sutsumprice, status) VALUES '+
			  '('+this.idbill+', \''+this.userem+'\', \''+this.sutsumpricebuy+'\', \''+this.idseller+'\','+
			  '\''+this.nameseller+'\', \''+this.dateimport+'\', \''+this.timeimpot+'\', \''+this.dateexport+'\','+
			  '\''+this.timeexport+'\', '+this.w_in_car+', '+this.w_out_car+', '+this.pricerefer+', '+this.percenbaby+','+
			  ''+this.precencal+', '+this.pricecal+', '+this.w_car+', '+this.sumprice+', '+this.sutsumprice+', 0);';
	request.query(sql, function(error, result){
		if(error){
			console.log("Error save bills to database"+error);
			return callback(error);
		}
		else{
			return callback("Save Complate");
		}
	});
}

exports.findAllIdBill = function(callback){
	var sql = 'SELECT [idbill] FROM ['+database+'].[dbo].[bills]';
	request.query(sql, function(err, returndata){
		if(err){
			console.log("Error Find Bill to database"+err);
			return callback(err);
		}
		else{
			return callback(returndata);
		}
	});
}

exports.findByAll = function(callback){
	var sql = 'SELECT * FROM ['+database+'].[dbo].[bills]';
	request.query(sql, function(err, returndata){
		if(err){
			console.log("Error Find All to database"+err);
			return callback(err);
		}
		else{
			return callback(returndata);
		}
	});
}

exports.findByID = function(callback){
	var sql = 'SELECT * FROM ['+database+'].[dbo].[bills] WHERE idbill LIKE \''+this.idbill+'\';';
	request.query(sql, function(err, recordset){
		if(err){
			console.log("Error FindByid to database"+err);
			return callback(err);
		}
		else{
			return callback(recordset);
		}
	});
}

exports.updatestatus = function(callback){
	var sql = 'UPDATE ['+database+'].[dbo].[bills] SET ['+database+'].[dbo].[bills].[status] = '+this.status+' WHERE ['+database+'].[dbo].[bills].[idbill] = '+this.idbill+';';
	request.query(sql, function(err, recordset){
		if(err){
			console.log("function updatestatus"+err);
			return callback(err);
		}
		else{
			return callback("Update Complate");
		}
	});
}