var fs = require('fs');
eval(fs.readFileSync('model/databaseconfig.js')+'');

var p30;
var p29;
var p28;
var p27;
var p26;
var p25;
var p24;
var p23;
var p22;
var p21;
var p20;
var p19;
var p18;
var p17;
var p16;
var p15;
var p14;
var p13;
var p12;

exports.save = function(callback){
	var sql = 	'INSERT INTO ['+database+'].[dbo].[price] (id, p30, p29, p28, p27, p26, p25, p24, p23, p22, p21, p20, p19, p18, p17, p16, p15, p14, p13, p12)'+
				'VALUES (1, $'+this.p30+', $'+this.p29+', $'+this.p28+', $'+this.p27+', $'+this.p26+', $'+this.p25+', $'+this.p24+','+
						'$'+this.p23+', $'+this.p22+', $'+this.p21+', $'+this.p20+', $'+this.p19+', $'+this.p18+', $'+this.p17+','+
						'$'+this.p16+', $'+this.p15+', $'+this.p14+', $'+this.p13+', $'+this.p12+');';
	request.query(sql, function(error, result){
		if(error){
			console.log("Error save price to database"+error);
			return callback(error);
		}
		else{
			return callback("Save Complate");
		}
	});
}

exports.findAll = function(callback){
	var sql = 'SELECT * FROM ['+database+'].[dbo].[price];';
	request.query(sql, function(err, recordset){
		if(err){
			console.log("Error Find price to database"+err);
			return callback(err);
		}
		else{
			return callback(recordset);
		}
	});
}

exports.deleteAll = function(callback){
	var sql = 'DELETE ['+database+'].[dbo].[price];';
	request.query(sql, function(err, recordset){
		if(err){
			console.log("Error DELETE price to database"+err);
			return callback(err);
		}
		else{
			return callback("DELETE Complate");
		}
	});
}