USE [project]
GO
/****** Object:  Table [dbo].[user]    Script Date: 04/22/2015 13:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[name] [varchar](100) NULL,
	[password] [varchar](100) NULL,
	[tel] [varchar](50) NULL,
	[address] [varchar](50) NULL,
	[username] [varchar](100) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[typegroup] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[stock]    Script Date: 04/22/2015 13:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stock](
	[tontoonaver] [float] NOT NULL,
	[namnang] [float] NOT NULL,
	[id] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sendproduct]    Script Date: 04/22/2015 13:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sendproduct](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idbills] [varchar](100) NULL,
	[namedrive] [varchar](100) NULL,
	[regiscar] [varchar](100) NULL,
	[namecus] [varchar](100) NULL,
	[datecusreciev] [int] NULL,
	[startpoi] [text] NULL,
	[stoppoi] [text] NULL,
	[direction] [float] NULL,
	[pricetran] [float] NULL,
	[weight] [float] NULL,
	[sumpricesell] [float] NULL,
	[sumall] [float] NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sendprice]    Script Date: 04/22/2015 13:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sendprice](
	[pricesend] [float] NOT NULL,
	[id] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sellprice]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sellprice](
	[id] [nchar](10) NULL,
	[priceforsell] [float] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[selller]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[selller](
	[idseller] [varchar](50) NULL,
	[name] [text] NULL,
	[address] [text] NULL,
	[tel] [varchar](50) NULL,
	[regiscar] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[price]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price](
	[p30] [money] NOT NULL,
	[p29] [money] NOT NULL,
	[p28] [money] NOT NULL,
	[p27] [money] NOT NULL,
	[p26] [money] NOT NULL,
	[p25] [money] NOT NULL,
	[p24] [money] NOT NULL,
	[p23] [money] NOT NULL,
	[p22] [money] NOT NULL,
	[p21] [money] NOT NULL,
	[p20] [money] NOT NULL,
	[p19] [money] NOT NULL,
	[p18] [money] NOT NULL,
	[p17] [money] NOT NULL,
	[p16] [money] NOT NULL,
	[p15] [money] NOT NULL,
	[p14] [money] NOT NULL,
	[p13] [money] NOT NULL,
	[p12] [money] NOT NULL,
	[id] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[plant]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[plant](
	[plant_id] [int] NOT NULL,
	[plant_name] [varchar](100) NULL,
	[price] [money] NULL,
	[status] [varchar](50) NULL,
 CONSTRAINT [PK_plant] PRIMARY KEY CLUSTERED 
(
	[plant_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[personnel]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[personnel](
	[personnel_id] [int] NOT NULL,
	[personnel_name] [varchar](100) NULL,
	[personnel_lastname] [varchar](100) NULL,
	[tel] [varchar](50) NULL,
	[address] [varchar](100) NULL,
	[salary] [varchar](100) NULL,
 CONSTRAINT [PK_personnel] PRIMARY KEY CLUSTERED 
(
	[personnel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[delivery]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[delivery](
	[delivery_id] [varbinary](50) NOT NULL,
	[plant_id] [varchar](50) NOT NULL,
	[personnel_id] [varchar](50) NOT NULL,
	[delivery date] [datetime] NULL,
 CONSTRAINT [PK_delivery] PRIMARY KEY CLUSTERED 
(
	[delivery_id] ASC,
	[plant_id] ASC,
	[personnel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[car]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[car](
	[registration] [varchar](50) NOT NULL,
	[type] [varchar](50) NULL,
	[goods_id] [varchar](50) NULL,
	[doods_weigt] [varchar](50) NULL,
 CONSTRAINT [PK_car] PRIMARY KEY CLUSTERED 
(
	[registration] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bills]    Script Date: 04/22/2015 13:53:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bills](
	[idbill] [int] NOT NULL,
	[userem] [varchar](50) NULL,
	[sutsumpricebuy] [varchar](50) NULL,
	[idseller] [varchar](50) NULL,
	[nameseller] [text] NULL,
	[dateimport] [date] NULL,
	[timeimpot] [time](7) NULL,
	[dateexport] [date] NULL,
	[timeexport] [time](7) NULL,
	[w_in_car] [float] NULL,
	[w_out_car] [float] NULL,
	[pricerefer] [float] NULL,
	[percenbaby] [float] NULL,
	[precencal] [int] NULL,
	[pricecal] [float] NULL,
	[w_car] [float] NULL,
	[sumprice] [float] NULL,
	[sutsumprice] [float] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_bills] PRIMARY KEY CLUSTERED 
(
	[idbill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
