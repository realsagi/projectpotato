//===================  ส่วนจัดการ รับซ์้อ ================================================
function findidseller(){
	$.post("/create-seller/checkidseller",
		{
			idseller: $('#idforseller').val()
		},
		function(returndata){
			$('#licencecar').val(returndata[0].regiscar);
			$('#nameforseller').val(returndata[0].name);
		}
	);
}
function printbill(){
	$('#billprint').printArea();
}
function percenrulor(){
	var datainput = $('#percenbabymid').val();
	if(!$.isNumeric(datainput)){
		alert("กรุณากรอกข้อมูลเป็นตัวเลขเท่านั้นค่ะ");
		$('#percenbabymid').focus();
		$('#percenbabymid').select();
	}
	else{
		$.post("/pricepotato/findall",
			{

			},
			function(dataretrun){
				var data = dataretrun[0];
				var dataformate = Math.floor(parseFloat(datainput));
				if(dataformate >=1 && dataformate <= 12){
					$('#percencal').val("12");
					$('#pricebuy').val(data.p12);
				}
				else if(dataformate >=30){
					$('#percencal').val("30");
					$('#pricebuy').val(data.p30);
				}
				else{
					$('#percencal').val(dataformate.toString());
					if(dataformate == 13){$('#pricebuy').val(data.p13);}
					else if(dataformate == 14){$('#pricebuy').val(data.p14);}
					else if(dataformate == 15){$('#pricebuy').val(data.p15);}
					else if(dataformate == 16){$('#pricebuy').val(data.p16);}
					else if(dataformate == 17){$('#pricebuy').val(data.p17);}
					else if(dataformate == 18){$('#pricebuy').val(data.p18);}
					else if(dataformate == 19){$('#pricebuy').val(data.p19);}
					else if(dataformate == 20){$('#pricebuy').val(data.p20);}
					else if(dataformate == 21){$('#pricebuy').val(data.p21);}
					else if(dataformate == 22){$('#pricebuy').val(data.p22);}
					else if(dataformate == 23){$('#pricebuy').val(data.p23);}
					else if(dataformate == 24){$('#pricebuy').val(data.p24);}
					else if(dataformate == 25){$('#pricebuy').val(data.p25);}
					else if(dataformate == 26){$('#pricebuy').val(data.p26);}
					else if(dataformate == 27){$('#pricebuy').val(data.p27);}
					else if(dataformate == 28){$('#pricebuy').val(data.p28);}
					else if(dataformate == 29){$('#pricebuy').val(data.p29);}	
				}
				calculateprice();
			}
		);
	}
}

function calculatecar(){
	if($('#w_in_car').val() != "" && $('#w_out_car').val() != ""){
		if(!$.isNumeric($('#w_in_car').val())){
			alert("กรุณากรอกข้อมูลเป็นตัวเลขเท่านั้นค่ะ");
			$('#w_in_car').focus();
			$('#w_in_car').select();
		}
		else if(!$.isNumeric($('#w_out_car').val())){
			alert("กรุณากรอกข้อมูลเป็นตัวเลขเท่านั้นค่ะ");
			$('#w_out_car').focus();
			$('#w_out_car').select();
		}
		else{
			var wincar = parseFloat($('#w_in_car').val());
			var woutcar = parseFloat($('#w_out_car').val());
			var sum = wincar - woutcar;
			$('#w_car').val(sum.toString());
			calculateprice();
		}
	}
}

function calculateprice(){
	if($('#pricebuy').val() != "" && $('#w_car').val() != ""){
		var pricebuy = parseFloat($('#pricebuy').val());
		var wcar = parseFloat($('#w_car').val());
		var sum = pricebuy*wcar;
		$('#sumpricbuy').val(sum.toString());
		$('#sutsumpricebuy').val(sum.toString());
	}
}

function loadnumbill(){
	$.post("/em/findAllIdbill", 
		{

		},
		function(data){
			if(data == ""){
				$('#numbill').val(1);
			}
			else{
				var max = 0;
				for(var i = 0 ; i < data.length ; i++){
					if(max < parseInt(data[i].idbill) ){
						max = parseInt(data[i].idbill);
					}
				}
				var aa = $('#numbill').val(++max);
			}
		}
	);
	document.getElementById("dateimport").valueAsDate = new Date();
	document.getElementById("dateexport").valueAsDate = new Date();
}

function logout(){
	window.location.href = "/logout";
}

function loadtime(){
	var time1 = formatTimeOfDay($.now());
	var time2 = formatTimeOfDay($.now()+(15*60000));
    $('#timeimport').val(time1);
	$('#timeexport').val(time2);
	setTimeout("loadtime()",1000);
}

function formatTimeOfDay(millisSinceEpoch) {
  var secondsSinceEpoch = (millisSinceEpoch / 1000) | 0;
  var secondsInDay = ((secondsSinceEpoch % 86400) + 86400) % 86400;
  var seconds = secondsInDay % 60;
  var minutes = ((secondsInDay / 60) | 0) % 60;
  var hours = (secondsInDay / 3600)+7 | 0;
  return hours + (minutes < 10 ? ":0" : ":")
      + minutes + (seconds < 10 ? ":0" : ":")
      + seconds;
}

function callpricerefer(){
	$.post("/pricepotato/findall",
		{

		},
		function(dataretrun){
			$('#pricerefer').val(dataretrun[0].p30);
		}
	);
}

function canclebill(){
	$('#licencecar').val("");
	$('#idforseller').val("");
	$('#nameforseller').val("");
	$('#w_in_car').val("");
	$('#w_out_car').val("");
	$('#percenbabymid').val("");
	$('#percencal').val("");
	$('#pricebuy').val("");
	$('#w_car').val("");
	$('#sumpricbuy').val("");
	$('#sutsumpricebuy').val("");
}

function savebill(){
	if($('#idforseller').val() == ""){
		alert("กรุณาระบุหมายเลขผู้ขายด้วยค่ะ");
		$('#idforseller').focus();
	}
	else if($('#idforseller').val() != "" && $('#nameforseller').val() == "" && $('#licencecar').val() == ""){
		alert("กรุณาระบุหมายเลขผู้ขายที่ถูกต้องด้วยค่ะ");
		$('#idforseller').focus();
		$('#idforseller').select();
	}
	else if($('#w_in_car').val() == ""){
		alert("กรุณาระบุน้ำหนักรถเข้าด้วยค่ะ");
		$('#w_in_car').focus();
	}
	else if($('#w_out_car').val() == ""){
		alert("กรุณาระบุน้ำหนักรถออกด้วยค่ะ");
		$('#w_out_car').focus();
	}
	else if($('#percenbabymid').val() == ""){
		alert("กรุณาระบุ % แป้งวัดด้วยค่ะ");
		$('#percenbabymid').focus();
	}
	else{
		$.post("/stock/findall",
			{

			},
			function(returndata){
				if(returndata == ""){
					$.post("/stock/save",
						{
							w_car: $('#w_car').val(),
							sumpricbuy: $('#pricebuy').val()
						},
						function(returndata){
							if(returndata == "Save Stcok Complate"){
								subsavebill();
							}
							else{
								alert(returndata);
							}
						}
					);
				}
				else{
					var stock_w = returndata[0].namnang;
					var stock_p = returndata[0].tontoonaver;
					var new_w = $('#w_car').val();
					var new_p = $('#pricebuy').val()
					var sumup = (stock_w * stock_p) + (new_w * new_p);
					var sumdown = parseFloat(stock_w) + parseFloat(new_w);
					var sumfinish = sumup/sumdown;
					$.post("/stock/deleteall",
						{

						},
						function(returndatadelete){
							if(returndatadelete == "DELETE Complate"){
								$.post("/stock/save",
									{
										w_car: sumdown,
										sumpricbuy: sumfinish
									},
									function(returndata){
										if(returndata == "Save Stcok Complate"){
											subsavebill();
										}
										else{
											alert(returndata);
										}
									}
								);
							}
						}
					);
				}
			}
		);
	}
}
function subsavebill(){
	$.post("/em/savebill",
			{
				user: $('#user').text(),
				licenecar: $('#licencecar').val(),
				idforseller: $('#idforseller').val(),
				nameforseller: $('#nameforseller').val(),
				w_in_car: $('#w_in_car').val(),
				w_out_car: $('#w_out_car').val(),
				percenbabymid: $('#percenbabymid').val(),
				percencal: $('#percencal').val(),
				pricebuy: $('#pricebuy').val(),
				w_car: $('#w_car').val(),
				sumpricbuy: $('#sumpricbuy').val(),
				sutsumpricebuy: $('#sutsumpricebuy').val(),
				numbill: $('#numbill').val(),
				dateimport: $('#dateimport').val(),
				timeimport: $('#timeimport').val(),
				dateexport: $('#dateexport').val(),
				timeexport: $('#timeexport').val(),
				pricerefer: $('#pricerefer').val()
			},
			function(dataretrun){
				if(dataretrun == "Save Complate"){
					alert("บันทึกข้อมูลเรียบร้อยแล้วค่ะ");
					canclebill();
					loadnumbill();
				}
			}
		);
}
//===================  ส่วนจัดการ รับซ์้อ ================================================
//=================== ส่วนจัดส่งไปโรงงาน ================
function savetran(){
	var nowdate = new Date();
	$.post("/stock/findall",
		{

		},
		function(retrundataw){
			if(retrundataw != ""){
				var stock_w = retrundataw[0].namnang;
				var sell_w = parseFloat($('#pricetoontemp').val());
				var sum_w = stock_w - sell_w;
				if(sum_w >= 0){
					$.post("/stock/updatenamnang",
						{	
							set_w: sum_w
						},
						function(wwretrun){
							if(wwretrun == "UPDATE Complate"){
								$.post("/sendproduct/save",
									{
										idbills: nowdate,
										namedrive: $('#namedriver').val(),
										regiscar: $('#regiscarforsend').val(),
										namecus: $('#namecustomerre').val(),
										datecusreciev: $('#dayforsend').val(),
										startpoi: $('#startpoi').val(),
										stoppoi: $('#stoptpoi').val(),
										direction: $('#waypoint').val(),
										pricetran: $('#pricetran').val(),
										sumpricetoon: $('#pricetoontemp').val(),
										sumpricesell: $('#priceselltemp').val(),
										sumall: $('#pricesumtemp').val(),
									},
									function(data){
										if(data == "Save Complate"){
											canclesend();
										}
										else{
											alert("ERROR "+data);
										}
									}
								);
							}
						}
					);
				}
				else{
					alert("สินค้าใน stock หมดแล้วค่ะ");
				}
			}
			else{
				alert("ไม่มีข้อมูลใน Stock ค่ะ");
			}
		}
	);
}
function setstartpoi(){
	$('#showlistbill').hide();
	$('#showmap').show();
}
function setstoptpoi(){
	$('#showlistbill').hide();
	$('#showmap').show();
}
function closemap(){
	$('#showlistbill').show();
	$('#showmap').hide();
}
function canclesend(){
	$('#namedriver').val("");
	$('#regiscarforsend').val("");
	$('#dayforsend').val("");
	$('#startpoi').val("");
	$('#stoptpoi').val("");
	$('#waypoint').val("");
	$('#pricetoontemp').val(0);
	$('#priceselltemp').val(0);
	$('#pricetran').val(0);
	$('#pricesumtemp').val(0);
	//$(':checkbox').prop("checked",false);
	initialize();
}
function loadalldatabill(){
	var sumwiegth = 0;
	var summoney = 0;
	$.post("/em/findByAll",
		{

		},
		function(returndata){
			for(var i = 0 ; i < returndata.length ; i++){
				if(returndata[i].status == 0){
					sumwiegth = sumwiegth+returndata[i].w_car;
					summoney = summoney+returndata[i].sumprice;
					$('#showlistbill > table').append(
						'<tr align="center">'+
							'<td>'+
								returndata[i].idbill+
							'</td>'+
							'<td>'+
								returndata[i].nameseller+
							'</td>'+
							'<td>'+
								returndata[i].w_car+
							'</td>'+
							'<td>'+
								returndata[i].sumprice+
							'</td>'+
							'<td>'+
								returndata[i].dateimport.substring(0,10)+
							'</td>'+
						'</tr>'
					);
				}
			}
			$('#showlistbill > table').append(
				'<tr align="center">'+
					'<td colspan="2" align="center">'+
						'รวม'+
					'</td>'+
					'<td>'+
						sumwiegth+
					'</td>'+
					'<td>'+
						summoney+
					'</td>'+
				'</tr>'
			);
		}
	);
}
//======================================================

function processprice(){
    var lengthtocheck = $(':checkbox:checked').length;
    var pricesellpull;
    $.post("/sellprice/findall",
		{

		},
		function(data){
			pricesellpull = data[0].priceforsell;
			var weigth = $('#pricetoontemp').val();
			var sumsell = 0;
			sumsell = sumsell+(weigth*pricesellpull);
			$('#priceselltemp').val(sumsell);
			sumsellall();
		}
	);
}
$(document).ready(function() {
	//loadnumbill();
	//loadtime();
	//callpricerefer();
	//loadalldatabill();
	//$("#loaddatacarpotato").load("/create-seller");
})