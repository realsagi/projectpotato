var map;
var myLatlng;
var geocoder;
var directionsDisplay;
var directionsService;
var marker1;
var marker2;
function initialize() {
  myLatlng = new google.maps.LatLng(14.969762, 102.091813);
  var mapOptions = {
      center: myLatlng,
      zoom: 7
  };
  geocoder = new google.maps.Geocoder();
  directionsDisplay = new google.maps.DirectionsRenderer();
  directionsService = new google.maps.DirectionsService();
  map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
  directionsDisplay.setMap(map);
  google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
    computeTotalDistance(directionsDisplay.getDirections());
  });
  createmarker();
}

function computeTotalDistance(result) {
  var total = 0;
  var myroute = result.routes[0];
  for (var i = 0; i < myroute.legs.length; i++) {
    total += myroute.legs[i].distance.value;
  }
  total = total / 1000.0;
  $('#waypoint').val(total);
  processpriceperkilometer(total);
}

function processpriceperkilometer(total){
  $.post("/priceforsend/findall",
    {

    },
    function(data){
      var priceperkilo = data[0].pricesend;
      var sum = priceperkilo*total;
      $('#pricetran').val(sum);
      sumsellall();
    }
  );
}

function sumsellall(){
  var tran = parseFloat($('#pricetran').val());
  var potatosell = parseFloat($('#priceselltemp').val());
  console.log("ค่าขนส่ง"+tran);
  console.log("ราคาขาย"+potatosell);
  var sum = tran + potatosell;
  console.log("รวม"+sum);
  $('#pricesumtemp').val(sum);
}

function createmarker(){
  marker1 = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'POINT STRAT',
      draggable:true,
      animation: google.maps.Animation.DROP
  });
  marker2 = new google.maps.Marker({
      position: myLatlng, 
      map: map,
      title: 'POINT STOP',
      draggable:true,
      animation: google.maps.Animation.DROP
  });
  google.maps.event.addListener(marker1, 'dragend', function() {
    geocoder.geocode(
        {
          'latLng': marker1.getPosition()
        }, 
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              $('#startpoi').val(results[1].formatted_address);
              calcRoute();
            }
        }
      }
    );
  });
  google.maps.event.addListener(marker2, 'dragend', function() {
    geocoder.geocode(
        {
          'latLng': marker2.getPosition()
        }, 
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              $('#stoptpoi').val(results[1].formatted_address);
              calcRoute();
            }
        }
      }
    );
  });
}

function calcRoute() {
  var start = marker1.getPosition();
  var end = marker2.getPosition();
  var request = {
      origin: new google.maps.LatLng(start.lat(),start.lng()),
      destination: new google.maps.LatLng(end.lat(),end.lng()),
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if(status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}
