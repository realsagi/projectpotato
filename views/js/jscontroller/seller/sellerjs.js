function saveseller(){
	if($('#idseller').val() == ""){
		alert("รหัสลูกค้าห้ามว่าง");
		$('#idseller').focus();
	}
	else if($('#nameseller').val() == ""){
		alert("ชื่อ-นามสกุลห้ามว่างค่ะ");
		$('#nameseller').focus();
	}
	else if($('#addressseller').val() == ""){
		alert("ที่อยู่ห้ามว่างค่ะ");
		$('#addressseller').focus();
	}
	else if($('#telseller').val() == ""){
		alert("เบอร์โทรห้ามว่างค่ะ");
		$('#telseller').focus();
	}
	else if($('#registercarseller').val() == ""){
		alert("ทะเบียนรถห้ามว่างค่ะ");
		$('#registercarseller').focus();
	}
	else{
		$.post("/create-seller/save",
			{
				idseller: $('#idseller').val(),
				nameseller: $('#nameseller').val(),
				addressseller: $('#addressseller').val(),
				telseller: $('#telseller').val(),
				registercarseller: $('#registercarseller').val()
			},
			function(retrundata){
				if(retrundata == "Save Complate"){
					alert("บันทึกเรียบร้อยค่ะ");
					cleartext();
				}
			}
		);
	}
}

function cleartext(){
	$('#idseller').val("");
	$('#nameseller').val("");
	$('#addressseller').val("");
	$('#telseller').val("");
	$('#registercarseller').val("");
}

function formattel(){
	var data = $('#telseller').val();
	if(data.length < 10){
		alert("กรุณาใส่เบอร์โทรให้ครบ 10 หลัก");
		$('#telseller').focus();
		$('#telseller').select();
	}
	else{
		var st1 = data.substring(0,3);
		var st2 = data.substring(3,10);
		$('#telseller').val(st1+"-"+st2);
	}
}

function checkidseller(){
	$.post("/create-seller/findbyidseller",
		{
			idseller: $('#idseller').val()
		},
		function(retrundata){
			if(retrundata == "no"){
				alert("ขออัยค่ะ ID นี้มีในระบบแล้วค่ะ");
				$('#idseller').val("");
				$('#idseller').focus();
			}
		}
	);
}