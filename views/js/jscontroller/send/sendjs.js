function savesenddata(){
	$.post("/priceforsend/save",
		{
			price: $('#priceforsenddata').val()
		},
		function(data){
			if(data == "Save Complate"){
				alert("บันทึกข้อมูลเรียบร้อยค่ะ")
			}
		}
	);
}
function loadpriceforsend(){
	$.post("/priceforsend/findall",
		{

		},
		function(data){
			$('#priceforsenddata').val(data[0].pricesend)
		}
	);
}

function savepricedata(){
	$.post("/sellprice/save",
		{
			price: $('#priceforsell').val()
		},
		function(data){
			if(data == "Save Complate"){
				alert("บันทึกข้อมูลเรียบร้อยค่ะ")
			}
		}
	);
}

function loadsellprice(){
	$.post("/sellprice/findall",
		{

		},
		function(data){
			$('#priceforsell').val(data[0].priceforsell)
		}
	);
}
function loadfindnamecus(){
	$.post("/admin/findbyFilter",
		{
			dataname: $('#namecustomerre').val()
		},
		function(data){
			var availableTags = [];
			var count = data.length;
			for(var i = 0 ; i < count ; i++){
				if(data[i].typegroup == "customer"){
					availableTags.push(data[i].name);
				}
			}
			$( "#namecustomerre" ).autocomplete({
				source: availableTags
			});
		}
	);	
}

function loadalldatabillsell(){
	$.post("/sendproduct/findall",
		{

		},
		function(returndata){
			var sumwiegth = 0;
			var summoney = 0;
			for(var i = 0 ; i < returndata.length ; i ++){
				sumwiegth = sumwiegth + returndata[i].weight;
				summoney = summoney + returndata[i].sumpricesell;
				$('#showlistbill2 > table').append(
						'<tr align="center">'+
							'<td>'+
								returndata[i].id+
							'</td>'+
							'<td>'+
								returndata[i].namecus+
							'</td>'+
							'<td>'+
								returndata[i].weight+
							'</td>'+
							'<td>'+
								returndata[i].sumpricesell+
							'</td>'+
							'<td>'+
								returndata[i].idbills.substring(0,10)+
							'</td>'+
						'</tr>'
				);
			}
			$('#showlistbill2 > table').append(
				'<tr align="center">'+
					'<td colspan="2" align="center">'+
						'รวม'+
					'</td>'+
					'<td>'+
						sumwiegth+
					'</td>'+
					'<td>'+
						summoney+
					'</td>'+
				'</tr>'
			);
		}
	);
}

function controllstatusproduct(){
	$.get("/login/checktypgroup",
		{

		},
		function(returngrouptype){
			var namegrouptpe = returngrouptype;
			$.post("/main/checksessionname",
				{

				},
				function(retrunsession){
					var sessionuser = retrunsession;
					$.post("/sendproduct/findall",
						{

						},
						function(retrundata){
							console.log(sessionuser);
							for(var i = 0 ; i < retrundata.length ; i++){
								if((retrundata[i].namecus == sessionuser) || (namegrouptpe == "admin" || namegrouptpe == "employee")){
									var pricepotato = retrundata[i].sumpricesell / retrundata[i].weight;
									if(retrundata[i].status == 0){
										$('#showlistsendproduct > table').append(
											'<tr align="center">'+
												'<td align="center">'+
													retrundata[i].idbills.substring(0,24)+
												'</td>'+
												'<td align="center">'+
													'อีก '+retrundata[i].datecusreciev+' วันจากวันส่ง'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].weight+' ก.ก.'+
												'</td>'+
												'<td align="center">'+
													'ก.ก.ละ '+pricepotato+' บาท'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].pricetran+' บาท'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].sumall+' บาท'+
												'</td>'+
												'<td>'+
														'<input type="checkbox" name="csp" value='+retrundata[i].id+'>'+
												'</td>'+
											'</tr>'
										);
									}
									else{
										$('#showlistsendproduct > table').append(
											'<tr align="center">'+
												'<td align="center">'+
													retrundata[i].idbills.substring(0,24)+
												'</td>'+
												'<td align="center">'+
													'อีก '+retrundata[i].datecusreciev+' วันจากวันส่ง'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].weight+' ก.ก.'+
												'</td>'+
												'<td align="center">'+
													'ก.ก.ละ '+pricepotato+' บาท'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].pricetran+' บาท'+
												'</td>'+
												'<td align="center">'+
													retrundata[i].sumall+' บาท'+
												'</td>'+
												'<td>'+
													'รับสินค้าแล้ว'+
												'</td>'+
											'</tr>'
										);
									}
								}
							}
						}
					);
				}
			);
		}
	);
}

function saveupdatestatus(){
    var searchIDs = $("input[name=csp]:checked").map(function(){

      return $(this).val();

    });
    console.log(searchIDs.get());
    if(searchIDs.get() != ""){
	    $.post("/sendproduct/updatestatus",
	    	{
	    		idq: searchIDs.get()
	    	},
	    	function(retrundata){
	    		if(retrundata == "UPDATE Complate"){
	    			alert("ทำการปรับทึกข้อมูลเรียบร้อยแล้วค่ะ");
	    			loadstatusproduct();
	    		}
	    		else{
	    			alert(retrundata);
	    		}
	    	}
	    );
	}
	else{
		alert("กรุณาเลือกรายการด้วยค่ะ");
	}
}